//[SECTION DATA MODELING]

//1. Identify what information we want to gather from the customers to indetify the user

//=> WHY? 
	//1. To plan what info would be useful
	//2. To lessen the chances or scenarious of having to modify the data stored in the databases
	//3. anticipate how this data/info would be related

//TASK: Create a Course Booking System for an Institution

	//What are the minimum info
	// 1. first name
	// 2. last Name 
	// 3. middle Name
	// 4. email address
	// 5. pin/pass
	// 6. mobile number
	// 7. bday
	// 8. gender
	// 9. isAdmin : role and restriction/limitation the user would have

	//Course/Subjects 

	    //1. name/title
	    //2. course code 
	    //3. course description
	    //4. course units
	    //5. course instructor
	    //6. isActive => to describe if the course is being offered by the institution. 
	    //7. dateTimeCreated => for us to identify when the course was added to the database. 
	    //8. available slots


     //As a full stack web developer (front, backend, database)
     //The more information you gather the more difficult it is to scale and manage the collection. it is going more difficult to maintain. 



//2. Create an ERD to represent the entities inside the database as well as to describe the relationships amongst them. 


     //users can have multiple subjects
     //subjects can have multiple enrollees

     //user can have multiple transactions
     //transaction can only belong to a single user. 

     //a single transaction can have multiple courses
     //a course can be part of mutiple transactions
    
	    //ill give you 5 minuteS to export your ERD_course_booking and save JPEG, PDF, PNG file inside the discussion folder. 

	    //send a screenshot or image of your ERD in hangouts GC. 

	    //WDC028-22 | MongoDB - Data Modeling and Translation -> link a copy of the project repo



//3. Convert and Translate the ERD into a JSON-like syntax in order to describe the structure of the document inside the collection by creating a MOCK data. 

      //NOTE: Creating/using mock data can play an integral role during the testing phase of any app development.

      //username: martin@email.com-> this is one one of the things you need to avoid.

      //you want to be able to utilize the storage in your database. 
	// https://www.mockaroo.com/
      
